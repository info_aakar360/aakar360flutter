import 'package:flutter/material.dart';

// list of colors that we use in our app
// const kBackgroundColor = Color(0xFFF0F5FC);
// const kPrimaryColor = Color(0xff003481);
// const kPrimaryDarkColor = Color(0xff003481);
// const kAccentColor = Color(0xff003481);
const kTextColor = Color(0xFF494949);
// const kTextLightColor = Color(0xFF747474);
// const kBlueColor = Color(0xFF40BAD5);
//
// //Colors For Project Status
// const kLightBlueColor = Color(0xffF0F6FE);
// const kGreenColor = Color(0xff4caf50);
// const kLightGreenColor = Color(0xffF2FFF3);
// const kGreyColor = Color(0xff494949);
// const kLightGreyColor = Color(0xffF4FFF3);
// const kYellowColor = Color(0xffFBC02D);
// const kLightYellowColor = Color(0xffFFFCF3);
// const kLightRedColor = Color(0xffF4E8E7);
// const kRedColor = Color(0xfff44336);
// const kGreen2Color = Color(0xff1b5e2b);
// const kLightGreen2Color = Color(0xffF1FFF6);
// const kLightRed2Color = Color(0xffFFF2F2);
// const kRed2Color = Color(0xffB71C1C);
// const kDarkBlue = Color(0xff001739);

const kTextStyle = TextStyle(color: kTextColor, fontFamily: 'Poppins');
const kBoldTextStyle = TextStyle(
    fontWeight: FontWeight.bold, color: kTextColor, fontFamily: 'Poppins');
const kSmallTextStyle =
    TextStyle(fontSize: 12, color: kTextColor, fontFamily: 'Poppins');

const kDefaultPadding = 20.0;

// our default Shadow
const kDefaultShadow = BoxShadow(
  offset: Offset(0, 15),
  blurRadius: 27,
  color: Colors.black12, // Black color with 12% opacity
);

enum AlertAction {
  cancel,
  discard,
  disagree,
  agree,
}

// const String baseApiUrl = "https://pmsdev.aakar360.com/api/";
// const String loginApiURL = baseApiUrl + "app-login";
// const String getProjectListApiURL = baseApiUrl + "app-projects";
// const String getIndentListApiURL = baseApiUrl + "indents-list";
// const String homepageApiURL = baseApiUrl + "homepage";
// const String API_KEY = "CXyp9xxyfOEd0mBR4XzuyHWjlu1YWEFM4ypZ1TEF";
// const bool devMode = false;
// const double textScaleFactor = 1.0;
// const String getBoqListApi = baseApiUrl + "boqlist";
// const String getLauncgProjectListApi = baseApiUrl + "launch-projects-list";
// const String getProjectStatusListApi = baseApiUrl + "project-status-list";
