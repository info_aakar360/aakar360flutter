class IndentModel {
  int id;
  String indentNo;
  int projectId;
  String projectName;
  String addedBy;
  Null remark;
  String status;
  String createdAt;

  IndentModel(
      {this.id,
      this.indentNo,
      this.projectId,
      this.projectName,
      this.addedBy,
      this.remark,
      this.status,
      this.createdAt});

  IndentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    indentNo = json['indent_no'];
    projectId = json['project_id'];
    projectName = json['project_name'];
    addedBy = json['added_by'];
    remark = json['remark'];
    status = json['status'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['indent_no'] = this.indentNo;
    data['project_id'] = this.projectId;
    data['project_name'] = this.projectName;
    data['added_by'] = this.addedBy;
    data['remark'] = this.remark;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    return data;
  }
}
