/// image : "https://pmsdev.aakar360.com/public/user-uploads/construction-4.jpg"

class New_image_model {
  String _image;

  String get image => _image;

  New_image_model({
      String image}){
    _image = image;
}

  New_image_model.fromJson(dynamic json) {
    _image = json["image"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["image"] = _image;
    return map;
  }

}