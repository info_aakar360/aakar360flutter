/// pagecount : 10
/// mode : "true"
/// errormsg : "App is in development mode till 6:00 PM to 9:00 PM"
/// images : [{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-4.jpg"},{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-5.jpg"},{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-6.jpg"}]
/// totalissuestoday : 0
/// totalindentstoday : 0
/// version_code : "1.0.7"
/// youtubelinks : [{"youtubeid":"m24CjND2Tgs","title":"Aakar360 project complete tutorial","link":"https://www.youtube.com/watch?v=m24CjND2Tgs"},{"youtubeid":"IR-QrykhJ5A","title":"Aakar360 प्रोजेक्ट कम्पलीट ट्यूटोरियल","link":"https://www.youtube.com/watch?v=IR-QrykhJ5A"},{"youtubeid":"MgHv7sw6IHM","title":"How to create project?","link":"https://www.youtube.com/watch?v=MgHv7sw6IHM"},{"youtubeid":"qtzHGj14UVs","title":"प्रोजेक्ट कैसे बनाएं?","link":"https://www.youtube.com/watch?v=qtzHGj14UVs"},{"youtubeid":"IoeKsvTlHPY","title":"How to make activity and task?","link":"https://www.youtube.com/watch?v=IoeKsvTlHPY"},{"youtubeid":"kfZcZgHpR2w","title":"एक्टिविटी और टास्क कैसे बनाये?","link":"https://www.youtube.com/watch?v=kfZcZgHpR2w"},{"youtubeid":"WfEInJJzeJM","title":"How to raise an issue?","link":"https://www.youtube.com/watch?v=WfEInJJzeJM"},{"youtubeid":"03jIjsgP7_M","title":"इशू कैसे रेज करें?","link":"https://www.youtube.com/watch?v=03jIjsgP7_M"},{"youtubeid":"wjb_fl7xB_I","title":"How to enter manpower log?","link":"https://www.youtube.com/watch?v=wjb_fl7xB_I"},{"youtubeid":"3L4MhmTYz04","title":"मैनपावर लॉग कैसे दर्ज करें?","link":"https://www.youtube.com/watch?v=3L4MhmTYz04&t=6s"},{"youtubeid":"t0MrVrnCGpY","title":"How to raise indent?","link":"https://www.youtube.com/watch?v=t0MrVrnCGpY"},{"youtubeid":"kVsH5lOR0Ao","title":"इंडेंट कैसे बनाये?","link":"https://www.youtube.com/watch?v=kVsH5lOR0Ao"},{"youtubeid":"bBI2ZLK83gg","title":"How to invite team member?","link":"https://www.youtube.com/watch?v=bBI2ZLK83gg"},{"youtubeid":"bBI2ZLK83gg","title":"टीम मेंबर को कैसे जोड़ें?","link":"https://www.youtube.com/watch?v=5Losnf2jjZM"}]

class Home_page_api_model {
  int _pagecount;
  String _mode;
  String _errormsg;
  List<Images> _images;
  int _totalissuestoday;
  int _totalindentstoday;
  String _versionCode;
  List<Youtubelinks> _youtubelinks;

  int get pagecount => _pagecount;
  String get mode => _mode;
  String get errormsg => _errormsg;
  List<Images> get images => _images;
  int get totalissuestoday => _totalissuestoday;
  int get totalindentstoday => _totalindentstoday;
  String get versionCode => _versionCode;
  List<Youtubelinks> get youtubelinks => _youtubelinks;

  Home_page_api_model({
      int pagecount, 
      String mode, 
      String errormsg, 
      List<Images> images, 
      int totalissuestoday, 
      int totalindentstoday, 
      String versionCode, 
      List<Youtubelinks> youtubelinks}){
    _pagecount = pagecount;
    _mode = mode;
    _errormsg = errormsg;
    _images = images;
    _totalissuestoday = totalissuestoday;
    _totalindentstoday = totalindentstoday;
    _versionCode = versionCode;
    _youtubelinks = youtubelinks;
}

  Home_page_api_model.fromJson(dynamic json) {
    _pagecount = json["pagecount"];
    _mode = json["mode"];
    _errormsg = json["errormsg"];
    if (json["images"] != null) {
      _images = [];
      json["images"].forEach((v) {
        _images.add(Images.fromJson(v));
      });
    }
    _totalissuestoday = json["totalissuestoday"];
    _totalindentstoday = json["totalindentstoday"];
    _versionCode = json["version_code"];
    if (json["youtubelinks"] != null) {
      _youtubelinks = [];
      json["youtubelinks"].forEach((v) {
        _youtubelinks.add(Youtubelinks.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["pagecount"] = _pagecount;
    map["mode"] = _mode;
    map["errormsg"] = _errormsg;
    if (_images != null) {
      map["images"] = _images.map((v) => v.toJson()).toList();
    }
    map["totalissuestoday"] = _totalissuestoday;
    map["totalindentstoday"] = _totalindentstoday;
    map["version_code"] = _versionCode;
    if (_youtubelinks != null) {
      map["youtubelinks"] = _youtubelinks.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// youtubeid : "m24CjND2Tgs"
/// title : "Aakar360 project complete tutorial"
/// link : "https://www.youtube.com/watch?v=m24CjND2Tgs"

class Youtubelinks {
  String _youtubeid;
  String _title;
  String _link;

  String get youtubeid => _youtubeid;
  String get title => _title;
  String get link => _link;

  Youtubelinks({
      String youtubeid, 
      String title, 
      String link}){
    _youtubeid = youtubeid;
    _title = title;
    _link = link;
}

  Youtubelinks.fromJson(dynamic json) {
    _youtubeid = json["youtubeid"];
    _title = json["title"];
    _link = json["link"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["youtubeid"] = _youtubeid;
    map["title"] = _title;
    map["link"] = _link;
    return map;
  }

}

/// image : "https://pmsdev.aakar360.com/public/user-uploads/construction-4.jpg"

class Images {
  String _image;

  String get image => _image;

  Images({
      String image}){
    _image = image;
}

  Images.fromJson(dynamic json) {
    _image = json["image"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["image"] = _image;
    return map;
  }

}