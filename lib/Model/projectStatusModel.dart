class ProjectStatusModel {
  int id;
  String slug;
  String name;
  bool checked = true;

  ProjectStatusModel({this.id, this.slug, this.name, this.checked});

  bool get isChecked => checked;

  ProjectStatusModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    slug = json['slug'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['slug'] = this.slug;
    data['name'] = this.name;
    return data;
  }
}
