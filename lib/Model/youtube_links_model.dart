// To parse this JSON data, do
//
//     final youTubeLinksModel = youTubeLinksModelFromMap(jsonString);

import 'dart:convert';

class YouTubeLinksModel {
  YouTubeLinksModel({
    this.youtubelinks,
  });

  List<Youtubelink> youtubelinks;

  factory YouTubeLinksModel.fromJson(String str) =>
      YouTubeLinksModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory YouTubeLinksModel.fromMap(Map<String, dynamic> json) =>
      YouTubeLinksModel(
        youtubelinks: List<Youtubelink>.from(
            json["youtubelinks"].map((x) => Youtubelink.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "youtubelinks": List<dynamic>.from(youtubelinks.map((x) => x.toMap())),
      };
}

class Youtubelink {
  Youtubelink({
    this.youtubeid,
    this.title,
    this.link,
  });

  String youtubeid;
  String title;
  String link;

  factory Youtubelink.fromJson(String str) =>
      Youtubelink.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Youtubelink.fromMap(Map<String, dynamic> json) => Youtubelink(
        youtubeid: json["youtubeid"],
        title: json["title"],
        link: json["link"],
      );

  Map<String, dynamic> toMap() => {
        "youtubeid": youtubeid,
        "title": title,
        "link": link,
      };
}
