class BoqModel {
  String module;
  int id;
  int itemid;
  String itemname;
  int level;
  Null catlevel;
  int count;
  int boqid;
  String category;
  int positionId;
  String heading;
  String startDate;
  String dueDate;
  int percentage;
  Null quantity;
  Null rate;
  String description;
  String contractorName;
  String projectName;
  String subprojectName;
  String segmentName;
  String assignTo;
  String assignToName;
  String status;

  BoqModel(
      {this.module,
      this.id,
      this.itemid,
      this.itemname,
      this.level,
      this.catlevel,
      this.count,
      this.boqid,
      this.category,
      this.positionId,
      this.heading,
      this.startDate,
      this.dueDate,
      this.percentage,
      this.quantity,
      this.rate,
      this.description,
      this.contractorName,
      this.projectName,
      this.subprojectName,
      this.segmentName,
      this.assignTo,
      this.assignToName,
      this.status});

  BoqModel.fromJson(Map<String, dynamic> json) {
    module = json['module'];
    id = json['id'];
    itemid = json['itemid'];
    itemname = json['itemname'];
    level = json['level'];
    catlevel = json['catlevel'];
    count = json['count'];
    boqid = json['boqid'];
    category = json['category'];
    positionId = json['position_id'];
    heading = json['heading'];
    startDate = json['start_date'];
    dueDate = json['due_date'];
    percentage = json['percentage'];
    quantity = json['quantity'];
    rate = json['rate'];
    description = json['description'];
    contractorName = json['contractor_name'];
    projectName = json['project_name'];
    subprojectName = json['subproject_name'];
    segmentName = json['segment_name'];
    assignTo = json['assign_to'];
    assignToName = json['assign_to_name'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['module'] = this.module;
    data['id'] = this.id;
    data['itemid'] = this.itemid;
    data['itemname'] = this.itemname;
    data['level'] = this.level;
    data['catlevel'] = this.catlevel;
    data['count'] = this.count;
    data['boqid'] = this.boqid;
    data['category'] = this.category;
    data['position_id'] = this.positionId;
    data['heading'] = this.heading;
    data['start_date'] = this.startDate;
    data['due_date'] = this.dueDate;
    data['percentage'] = this.percentage;
    data['quantity'] = this.quantity;
    data['rate'] = this.rate;
    data['description'] = this.description;
    data['contractor_name'] = this.contractorName;
    data['project_name'] = this.projectName;
    data['subproject_name'] = this.subprojectName;
    data['segment_name'] = this.segmentName;
    data['assign_to'] = this.assignTo;
    data['assign_to_name'] = this.assignToName;
    data['status'] = this.status;
    return data;
  }
}
