/// images : [{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-4.jpg"},{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-5.jpg"},{"image":"https://pmsdev.aakar360.com/public/user-uploads/construction-6.jpg"}]

class SliderImageModel {
  List<Images> _images;

  List<Images> get images => _images;

  SliderImageModel({
      List<Images> images}){
    _images = images;
}

  SliderImageModel.fromJson(dynamic json) {
    if (json["images"] != null) {
      _images = [];
      json["images"].forEach((v) {
        _images.add(Images.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_images != null) {
      map["images"] = _images.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// image : "https://pmsdev.aakar360.com/public/user-uploads/construction-4.jpg"

class Images {
  String _image;

  String get image => _image;

  Images({
      String image}){
    _image = image;
}

  Images.fromJson(dynamic json) {
    _image = json["image"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["image"] = _image;
    return map;
  }

}