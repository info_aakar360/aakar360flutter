/// id : 1
/// company_id : 2
/// project_name : "CM house Construction"
/// project_summary : null
/// project_admin : 3
/// start_date : "2021-02-20"
/// deadline : "2021-03-20"
/// notes : null
/// category_id : 3
/// subproject : "disable"
/// segment : "disable"
/// client_id : null
/// feedback : null
/// manual_timelog : "disable"
/// client_view_task : "disable"
/// allow_client_notification : "disable"
/// completion_percent : 0
/// calculate_task_progress : "true"
/// created_at : "2021-02-20 13:33:04"
/// updated_at : "2021-02-20 13:33:04"
/// deleted_at : null
/// project_budget : null
/// currency_id : null
/// hours_allocated : null
/// status : "not started"
/// image : null
/// added_by : 3
/// imageurl : "https://www.achahomes.com/wp-content/uploads/2017/12/Indian-Style-Inspired-House-Design-1.jpg"
/// statusname : "Not Started"

class ProjectModel {
  int _id;
  int _companyId;
  String _projectName;
  dynamic _projectSummary;
  int _projectAdmin;
  String _startDate;
  String _deadline;
  dynamic _notes;
  int _categoryId;
  String _subproject;
  String _segment;
  dynamic _clientId;
  dynamic _feedback;
  String _manualTimelog;
  String _clientViewTask;
  String _allowClientNotification;
  int _completionPercent;
  String _calculateTaskProgress;
  String _createdAt;
  String _updatedAt;
  dynamic _deletedAt;
  dynamic _projectBudget;
  dynamic _currencyId;
  dynamic _hoursAllocated;
  String _status;
  dynamic _image;
  int _addedBy;
  String _imageurl;
  String _statusname;
  bool _isChecked = false;

  int get id => _id;
  int get companyId => _companyId;
  String get projectName => _projectName;
  dynamic get projectSummary => _projectSummary;
  int get projectAdmin => _projectAdmin;
  String get startDate => _startDate;
  String get deadline => _deadline;
  dynamic get notes => _notes;
  int get categoryId => _categoryId;
  String get subproject => _subproject;
  String get segment => _segment;
  dynamic get clientId => _clientId;
  dynamic get feedback => _feedback;
  String get manualTimelog => _manualTimelog;
  String get clientViewTask => _clientViewTask;
  String get allowClientNotification => _allowClientNotification;
  int get completionPercent => _completionPercent;
  String get calculateTaskProgress => _calculateTaskProgress;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  dynamic get deletedAt => _deletedAt;
  dynamic get projectBudget => _projectBudget;
  dynamic get currencyId => _currencyId;
  dynamic get hoursAllocated => _hoursAllocated;
  String get status => _status;
  dynamic get image => _image;
  int get addedBy => _addedBy;
  String get imageurl => _imageurl;
  String get statusname => _statusname;
  bool get isChecked => _isChecked;

  ProjectModel(
      {int id,
      int companyId,
      String projectName,
      dynamic projectSummary,
      int projectAdmin,
      String startDate,
      String deadline,
      dynamic notes,
      int categoryId,
      String subproject,
      String segment,
      dynamic clientId,
      dynamic feedback,
      String manualTimelog,
      String clientViewTask,
      String allowClientNotification,
      int completionPercent,
      String calculateTaskProgress,
      String createdAt,
      String updatedAt,
      dynamic deletedAt,
      dynamic projectBudget,
      dynamic currencyId,
      dynamic hoursAllocated,
      String status,
      dynamic image,
      int addedBy,
      String imageurl,
      String statusname,
      bool isChecked}) {
    _id = id;
    _companyId = companyId;
    _projectName = projectName;
    _projectSummary = projectSummary;
    _projectAdmin = projectAdmin;
    _startDate = startDate;
    _deadline = deadline;
    _notes = notes;
    _categoryId = categoryId;
    _subproject = subproject;
    _segment = segment;
    _clientId = clientId;
    _feedback = feedback;
    _manualTimelog = manualTimelog;
    _clientViewTask = clientViewTask;
    _allowClientNotification = allowClientNotification;
    _completionPercent = completionPercent;
    _calculateTaskProgress = calculateTaskProgress;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _deletedAt = deletedAt;
    _projectBudget = projectBudget;
    _currencyId = currencyId;
    _hoursAllocated = hoursAllocated;
    _status = status;
    _image = image;
    _addedBy = addedBy;
    _imageurl = imageurl;
    _statusname = statusname;
    _isChecked = isChecked;
  }

  ProjectModel.fromJson(dynamic json) {
    _id = json["id"];
    _companyId = json["company_id"];
    _projectName = json["project_name"];
    _projectSummary = json["project_summary"];
    _projectAdmin = json["project_admin"];
    _startDate = json["start_date"];
    _deadline = json["deadline"];
    _notes = json["notes"];
    _categoryId = json["category_id"];
    _subproject = json["subproject"];
    _segment = json["segment"];
    _clientId = json["client_id"];
    _feedback = json["feedback"];
    _manualTimelog = json["manual_timelog"];
    _clientViewTask = json["client_view_task"];
    _allowClientNotification = json["allow_client_notification"];
    _completionPercent = json["completion_percent"];
    _calculateTaskProgress = json["calculate_task_progress"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _deletedAt = json["deleted_at"];
    _projectBudget = json["project_budget"];
    _currencyId = json["currency_id"];
    _hoursAllocated = json["hours_allocated"];
    _status = json["status"];
    _image = json["image"];
    _addedBy = json["added_by"];
    _imageurl = json["imageurl"];
    _statusname = json["statusname"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["company_id"] = _companyId;
    map["project_name"] = _projectName;
    map["project_summary"] = _projectSummary;
    map["project_admin"] = _projectAdmin;
    map["start_date"] = _startDate;
    map["deadline"] = _deadline;
    map["notes"] = _notes;
    map["category_id"] = _categoryId;
    map["subproject"] = _subproject;
    map["segment"] = _segment;
    map["client_id"] = _clientId;
    map["feedback"] = _feedback;
    map["manual_timelog"] = _manualTimelog;
    map["client_view_task"] = _clientViewTask;
    map["allow_client_notification"] = _allowClientNotification;
    map["completion_percent"] = _completionPercent;
    map["calculate_task_progress"] = _calculateTaskProgress;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["deleted_at"] = _deletedAt;
    map["project_budget"] = _projectBudget;
    map["currency_id"] = _currencyId;
    map["hours_allocated"] = _hoursAllocated;
    map["status"] = _status;
    map["image"] = _image;
    map["added_by"] = _addedBy;
    map["imageurl"] = _imageurl;
    map["statusname"] = _statusname;
    return map;
  }

  set checked(bool val) => _isChecked = val;
}
