import 'package:flutter/material.dart';
class MyColors{
// list of colors that we use in our app
   static const kBackgroundColor = Color(0xFFF0F5FC);
  static const kPrimaryColor = Color(0xff003481);
  static const kPrimaryDarkColor = Color(0xff003481);
  static const kAccentColor = Color(0xff003481);
  static const kTextColor = Color(0xFF494949);
  static const kTextLightColor = Color(0xFF747474);
  static const kBlueColor = Color(0xFF40BAD5);

//Colors For Project Status
  static const kLightBlueColor = Color(0xffF0F6FE);
  static const kGreenColor = Color(0xff4caf50);
  static const kLightGreenColor = Color(0xffF2FFF3);
  static const kGreyColor = Color(0xff494949);
  static const kLightGreyColor = Color(0xffF4FFF3);
  static const kYellowColor = Color(0xffFBC02D);
  static const kLightYellowColor = Color(0xffFFFCF3);
  static const kLightRedColor = Color(0xffF4E8E7);
  static const kRedColor = Color(0xfff44336);
  static const kGreen2Color = Color(0xff1b5e2b);
  static const kLightGreen2Color = Color(0xffF1FFF6);
  static const kLightRed2Color = Color(0xffFFF2F2);
  static const kRed2Color = Color(0xffB71C1C);
  static const kDarkBlue = Color(0xff001739);
}