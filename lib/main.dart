import 'package:aakar360flutter/src/login_page.dart';
import 'package:flutter/material.dart';
import 'package:aakar360flutter/Theme/myTheme.dart' as Theme;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aakar 360 Project',
      theme: Theme.MyThemeData,
      debugShowCheckedModeBanner: false,
      home: LoginPage(title: 'Flutter Demo Home Page'),
    );
  }
}
