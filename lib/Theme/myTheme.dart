import 'package:aakar360flutter/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:aakar360flutter/Theme/myTheme.dart' as Theme;

final ThemeData MyThemeData = new ThemeData(
    primaryColor: MyColors.kPrimaryColor,
    primaryColorDark: MyColors.kPrimaryDarkColor,
    primaryColorLight: MyColors.kPrimaryColor,
    accentColor: Colors.blue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    fontFamily: 'Poppins');
