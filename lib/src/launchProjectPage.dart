import 'dart:collection';
import 'dart:convert';

import 'package:aakar360flutter/Common/PreferenceManager.dart';
import 'package:aakar360flutter/Common/customDialog.dart';
import 'package:aakar360flutter/res/ApiClass.dart';
import 'package:aakar360flutter/res/colors.dart';
import 'package:aakar360flutter/src/login_page.dart';
import 'package:aakar360flutter/Model/project_model.dart';
import 'package:aakar360flutter/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'Filter/selectProjectFilterPage.dart';

class LaunchProjectPage extends StatefulWidget {
  final String filter;
  LaunchProjectPage({Key key, this.filter}) : super(key: key);

  @override
  _LaunchProjectPageState createState() => _LaunchProjectPageState();
}

class _LaunchProjectPageState extends State<LaunchProjectPage> {
  List<ProjectModel> projectList = new List();
  bool isLoading = false;
  bool isAllChecked = false;
  String checkedProjectIds = "";
  bool isSharedProject = false;
  String userCompanyId = "";
  bool performFiltering = false;

  final Widget errorIcon = new SvgPicture.asset(
    'assets/Icon/close.svg',
    width: 40,
    height: 40,
  );
  final Widget successIcon = new SvgPicture.asset(
    'assets/Icon/check.svg',
    width: 40,
    height: 40,
  );

  @override
  void initState() {
    super.initState();
    _getProjectList();
  }

  Future<void> _getProjectList() async {
    try {
      setState(() {
        isLoading = true;
      });

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      final jsonResponse = await http.post(ApiClass.getLauncgProjectListApi, body: {
        'API_KEY': ApiClass.API_KEY,
        'token': sharedPreferences.getString('token'),
        // 'project_id': "1",
      });
      Map data = new HashMap();
      data['API_KEY'] =ApiClass.API_KEY;
      // data['project_id'] = "1";
      data['token'] = sharedPreferences.getString('token');
      print(data);
      print(ApiClass.getProjectListApiURL);
      print(jsonResponse.body.toString());
      if (jsonResponse.statusCode == 200) {
        var message = json.decode(jsonResponse.body)['message'];
        String status = json.decode(jsonResponse.body)['status'].toString();
        if (status == "200") {
          print(status);
          final jsonItems = json
              .decode(jsonResponse.body)['response']
              .cast<Map<String, dynamic>>();

          projectList = jsonItems.map<ProjectModel>((json) {
            return ProjectModel.fromJson(json);
          }).toList();
          setState(() {
            isLoading = false;
          });
          // showInSnackBar("Data fetched please wait.", successIcon);
        } else if (status == "401") {
          sharedPreferences.clear();
          showInSnackBar("Please Login Again", errorIcon);
          Navigator.of(context).pushReplacement(
              // MaterialPageRoute(builder: (context) => HomePage()))
              MaterialPageRoute(builder: (context) => LoginPage()));
          setState(() {
            isLoading = false;
          });
        } else {
          setState(() {
            isLoading = false;
          });
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => CustomDialogBox(
              title: "Error",
              descriptions: message,
              text: "OK",
            ),
          );
        }
      } else {
        showInSnackBar("Failed to load data.", errorIcon);
        setState(() {
          isLoading = false;
        });
      }
    } on Exception catch (e) {
      print('getProjectList===> $e');
    }
  }

  void showInSnackBar(String value, Widget icon) {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
      duration: Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      backgroundColor: MyColors.kDarkBlue,
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0.0),
        child: Row(
          children: <Widget>[
            icon,
            Container(
                padding: EdgeInsets.only(left: 10.0),
                child: Text(
                  value,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ))
          ],
        ),
      ),
    ));
  }

  void checkAllProjects(bool isAllChecked) {
    print('checkAllProjects CALLED');
    print('PROJECTLIST SIZE');
    print(projectList.length);
    if (isAllChecked) {
      if (projectList.length > 0) {
        for (var i = 0; i < projectList.length; i++) {
          // setState(() {
          projectList[i].checked = true;
          // });
        }
      }
    } else {
      if (projectList.length > 0) {
        for (var i = 0; i < projectList.length; i++) {
          // setState(() {
          projectList[i].checked = false;
          // });
        }
      }
    }
  }

  Future<void> handleProceed() async {
    List<String> checkedArray = new List<String>();
    for (int i = 0; i < projectList.length; i++) {
      if (projectList[i].isChecked) {
        checkedArray.add(projectList[i].id.toString());
      }
    }

    if (checkedArray.length == 0) {
      showInSnackBar("No project selected", errorIcon);
      return;
    }

    String strChecked = checkedArray.join(",");
    // PreferenceManager preferenceManager = new PreferenceManager();
    // preferenceManager.setLaunchProjectList(strChecked);
    // print("LAUNCH_PROJECT_LIST");
    // print(preferenceManager.getLaunchProjectList());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('LAUNCH_PROJECT_LIST', strChecked);
    print(sharedPreferences.getString('LAUNCH_PROJECT_LIST'));

    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => HomePage()),
    // );
  }

  void _openFilterPage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SelectProjectFilterPage()),
    );
  }

  bool _isSharedProject(int companyId) {
    bool val = false;
    if (userCompanyId == companyId.toString()) {
      val = true;
    }
    return val;
  }

  @override
  Widget build(BuildContext context) {
    final processButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff003481),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          handleProceed();
        },
        child: Text("Proceed",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: "Poppins",
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
    );

    return Container(
      child: Scaffold(
        backgroundColor: MyColors.kBackgroundColor,
        appBar: AppBar(
          backgroundColor: MyColors.kPrimaryColor,
          title: Text(
            'Select Projects',
            style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontFamily: 'Poppins'),
          ),

          automaticallyImplyLeading: false,
          // this will hide Drawer hamburger icon
          actions: <Widget>[
            Container(),
          ],
          iconTheme: new IconThemeData(color: Colors.white),
        ),
        body: (isLoading == true && projectList.length == 0)
            ? Center(child: CircularProgressIndicator())
            : Container(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: EdgeInsets.only(left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            child: FlatButton(
                              color: Colors.blue[100],
                              onPressed: () {
                                _openFilterPage();
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Row(children: <Widget>[
                                Container(
                                    margin: EdgeInsets.only(right: 5.0),
                                    child: SvgPicture.asset(
                                      'assets/Icon/filter_icon.svg',
                                      color: MyColors.kPrimaryColor,
                                      height: 20.0,
                                      width: 20.0,
                                    )),
                                Text(
                                  'FILTER',
                                  style: TextStyle(
                                      color: MyColors.kPrimaryColor,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.bold),
                                ),
                              ]),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text('All Projects'),
                                  Container(
                                    margin: EdgeInsets.only(right: 30.0),
                                    child: Checkbox(
                                        value: isAllChecked,
                                        onChanged: (value) {
                                          setState(() {
                                            isAllChecked = value;
                                          });
                                          checkAllProjects(isAllChecked);
                                        }),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      )),
                  Expanded(
                    child: ListView.builder(
                      itemCount: projectList.length,
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          margin: const EdgeInsets.only(
                              top: 10, left: 20, right: 20, bottom: 10),
                          // child:
                          // InkWell(
                          //   onTap: () {
                          //     setState(() {
                          //       projectList[index].checked = true;
                          //     });
                          //   },
                          child: Stack(children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 0.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 0.0),
                                      margin: EdgeInsets.only(
                                          left: 8,
                                          right: 8,
                                          top: 15.0,
                                          bottom: 15.0),
                                      child: CachedNetworkImage(
                                        height: 70,
                                        width: 60,
                                        imageUrl: projectList[index].imageurl,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          width: 80.0,
                                          height: 80.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8.0)),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          projectList[index].projectName,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontFamily: 'Poppins',
                                            fontSize: 16.0,
                                          ),
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 3.0, horizontal: 10)),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: MyColors.kLightGreen2Color,
                                              border: Border.all(
                                                // color: Color(0xffF4E8E7),
                                                color: MyColors.kLightGreen2Color,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(3))),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 3.0, horizontal: 5),
                                            child: Text(
                                              projectList[index]
                                                  .status
                                                  .toUpperCase(),
                                              style: const TextStyle(
                                                  letterSpacing: 1,
                                                  fontSize: 8.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: MyColors.kGreen2Color),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                        // padding: EdgeInsets.only(
                                        //     top: 10, bottom: 10, right: 10),
                                        child: Container(
                                      // height: 20.0,
                                      margin:
                                          const EdgeInsets.only(right: 10.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Theme(
                                            data: ThemeData(
                                                unselectedWidgetColor:
                                                    Colors.blue[900]),
                                            child: Checkbox(
                                              value:
                                                  projectList[index].isChecked,
                                              checkColor: Colors.blue[900],
                                              activeColor: Colors.white,
                                              onChanged: (value) {
                                                setState(() {
                                                  projectList[index].checked =
                                                      value;
                                                });
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: (_isSharedProject(
                                  projectList[index].companyId)),
                              child: Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                    padding: EdgeInsets.only(
                                        left: 5.0,
                                        right: 5.0,
                                        top: 2.0,
                                        bottom: 2.0),
                                    decoration: BoxDecoration(
                                        color: Colors.green,
                                        border: Border.all(
                                          // color: Color(0xffF4E8E7),
                                          color: Colors.green,
                                        ),
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(10.0))),
                                    child: Text(
                                      'Shared'.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    )),
                              ),
                            ),
                          ]),
                          // ),
                        );
                      },
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    padding: const EdgeInsets.only(
                        top: 10.0, bottom: 5.0, left: 10.0, right: 10.0),
                    child: processButton,
                  )
                ],
              )),
      ),
    );
  }
}

class _ProjectItem extends StatefulWidget {
  const _ProjectItem(
      {Key key,
      this.projectId,
      this.title,
      this.status,
      this.imageUrl,
      this.isChecked})
      : super(key: key);

  final String title;
  final String status;
  final String imageUrl;
  final int projectId;
  final bool isChecked;
  @override
  __ProjectItemState createState() => __ProjectItemState();
}

class __ProjectItemState extends State<_ProjectItem> {
  @override
  Widget build(BuildContext context) {
    bool isChecked = false;
    Widget isCheckedCheckbox() {
      return Container(
        height: 20.0,
        margin: const EdgeInsets.only(bottom: 10.0),
        child: Row(
          children: <Widget>[
            Theme(
              data: ThemeData(unselectedWidgetColor: Colors.blue[900]),
              child: Checkbox(
                value: widget.isChecked,
                checkColor: Colors.blue[900],
                activeColor: Colors.white,
                onChanged: (value) {
                  setState(() {
                    isChecked = value;
                  });
                },
              ),
            ),
          ],
        ),
      );
    }

    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      margin: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
      child: InkWell(
        onTap: () {
          setState(() {
            isChecked = true;
          });
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 8, right: 8),
                  child: CachedNetworkImage(
                    height: 70,
                    width: 60,
                    imageUrl: widget.imageUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 80.0,
                      height: 80.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.title,
                      style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Poppins',
                        fontSize: 16.0,
                      ),
                    ),
                    const Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 3.0, horizontal: 10)),
                    Container(
                      decoration: BoxDecoration(
                          color: MyColors.kLightGreen2Color,
                          border: Border.all(
                            // color: Color(0xffF4E8E7),
                            color: MyColors.kLightGreen2Color,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 3.0, horizontal: 5),
                        child: Text(
                          widget.status.toUpperCase(),
                          style: const TextStyle(
                              letterSpacing: 1,
                              fontSize: 8.0,
                              fontWeight: FontWeight.bold,
                              color: MyColors.kGreen2Color),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10, right: 10),
                child: isCheckedCheckbox(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
