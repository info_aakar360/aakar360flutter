import 'package:aakar360flutter/main.dart';
import 'package:aakar360flutter/src/newpassword_page.dart';
import 'package:flutter/material.dart';
import 'package:aakar360flutter/src/signup_page.dart';

void main() => runApp(MyApp());

class MyOtpPage extends StatefulWidget {
  MyOtpPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyOtpPage createState() => _MyOtpPage();
}

class _MyOtpPage extends State<MyOtpPage> {
  bool _rememberMe = false;

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 13.0);
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    void _onLoading() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: new Text("Loading"),
                  ),
                ],
              ),
            ),
          );
        },
      );
      new Future.delayed(new Duration(seconds: 3), () {
        Navigator.of(context).pop(true); //pop dialog
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MyNewPasswordPage()),
        );      });
    }

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff006400),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _onLoading();
          }
        },
        child: Text("Submit",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    Widget otpBoxBuilder() {
      return Padding(
        padding: const EdgeInsets.all(3.0),
        child: Container(
          alignment: Alignment.center,
          height: 40,
          width: 40,
          child: TextField(
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black),
            keyboardType: TextInputType.number,
            maxLength: 1,
            decoration:
                InputDecoration(border: InputBorder.none, counterText: ''),
            textAlign: TextAlign.center,
            obscureText: true,
          ),
          decoration:
              BoxDecoration(border: Border.all(color: Colors.grey, width: 3)),
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Center(
          child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0, left:20.0),
                    child: IconButton(
                        icon: new Icon(Icons.arrow_back),
                        onPressed: () {
                          _backbuttonPressed();
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left:30.0, right:30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Please Enter OTP',
                          style: TextStyle(
                              fontSize: 20.0, fontFamily: 'Montserrat'),
                        ),
                        Container(
                          height: 250.0,
                          child: Image.asset(
                            "assets/images/otp_image.png",
                            fit: BoxFit.fill,
                          ),
                        ),
                        Form(
                            key: _formKey,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(height: 20.0),
                                  otpBoxBuilder(),
                                  otpBoxBuilder(),
                                  otpBoxBuilder(),
                                  otpBoxBuilder(),
                                  otpBoxBuilder(),
                                  otpBoxBuilder()
                                ])),
                        SizedBox(height: 20.0),
                        loginButon,
                      ],
                    ),
                  ),
                ],
              ),
          ),
        ),
      ),
    );
  }

  void _backbuttonPressed() {
    Navigator.of(context).pop(true);
  }
}
