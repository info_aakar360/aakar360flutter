import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:aakar360flutter/constants.dart';
import 'package:aakar360flutter/main.dart';
import 'package:aakar360flutter/res/ApiClass.dart';
import 'package:aakar360flutter/res/colors.dart';
import 'package:aakar360flutter/src/forgot_password_page.dart';
import 'package:aakar360flutter/src/launchProjectPage.dart';
import 'package:flutter/material.dart';
import 'package:aakar360flutter/src/signup_page.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _rememberMe = false;
  String _status = 'no-action';
  TextStyle style = TextStyle(fontSize: 13.0);
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _username, _password;
  TextEditingController _controllerUsername, _controllerPassword;
  bool _isLoading = false;

  SharedPreferences sharedPreferences;

  @override
  initState() {
    print("LoginPage initState Called");
    SharedPreferences.getInstance().then((prefValue) => {
          if (prefValue.getString('status') == "1")
            {
// wrong call in wrong place!
              Navigator.of(context).pushReplacement(
                  // MaterialPageRoute(builder: (context) => HomePage()))
                  MaterialPageRoute(builder: (context) => LaunchProjectPage()))
            }
        });

    _controllerUsername = TextEditingController();
    _controllerPassword = TextEditingController();
    // _loadUsername();
    super.initState();
    print(_status);
  }

  // void _loadUsername() async {
  //   try {
  //     SharedPreferences _prefs = await SharedPreferences.getInstance();
  //     var _username = _prefs.getString("saved_username") ?? "";
  //     var _remeberMe = _prefs.getBool("remember_me") ?? false;
  //
  //     if (_remeberMe) {
  //       _controllerUsername.text = _username ?? "";
  //     }
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  void _handleSubmitted() async {
    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      showInSnackBarNew('Please fix the errors in red before submitting.');
    } else {
      form.save();
      setState(() {
        _isLoading = true;
      });
      // _apiResponse = await
      authenticateUser(_username, _password);
    }
  }

  void showInSnackBarNew(String value) {
    print("showInSnackBarNew  Called");
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void authenticateUser(String username, String password) async {
    sharedPreferences = await SharedPreferences.getInstance();
    // PreferenceManager preferenceManager = new PreferenceManager();
    // try {
    final response = await http.post(ApiClass.loginApiURL, body: {
      'API_KEY': ApiClass.API_KEY,
      'email': username,
      'password': password,
      'fcm': "hhjvjvvhkhvyyvhbhv",
    });

    Map data = new HashMap();
    data['API_KEY'] = ApiClass.API_KEY;
    data['email'] = username;
    data['password'] = password;
    data['fcm'] = "hhjvjvvhkhvyyvhbhv";
    print(data);
    print(ApiClass.loginApiURL);
    print(response.body.toString());
    var jsonResponse = null;
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        print("message====>" + jsonResponse['message'].toString());
        print("status====>" + jsonResponse['status'].toString());
        if (jsonResponse['status'].toString() == "200") {
          print("token====>" + jsonResponse['response']['token'].toString());

          // preferenceManager
          //     .setAuthToken(jsonResponse['response']['token'].toString());
          // preferenceManager.setStatus(jsonResponse['response']['1'].toString());
          // preferenceManager
          //     .setUserId(jsonResponse['response']['id'].toString());
          // preferenceManager
          //     .setUserName(jsonResponse['response']['name'].toString());
          // preferenceManager
          //     .setEmail(jsonResponse['response']['email'].toString());
          // preferenceManager
          //     .setMobile(jsonResponse['response']['mobile'].toString());

          sharedPreferences.setString("status", "1");
          sharedPreferences.setString(
              "token", jsonResponse['response']['token'].toString());
          sharedPreferences.setString(
              "id", jsonResponse['response']['id'].toString());
          sharedPreferences.setString(
              "name", jsonResponse['response']['name'].toString());
          sharedPreferences.setString(
              "email", jsonResponse['response']['email'].toString());
          sharedPreferences.setString(
              "company_id", jsonResponse['response']['company_id'].toString());
          sharedPreferences.setString(
              "image", jsonResponse['response']['image'].toString());
          sharedPreferences.setString(
              "image_url", jsonResponse['response']['image_url'].toString());
          sharedPreferences.setString(
              "mobile", jsonResponse['response']['mobile'].toString());
          sharedPreferences.setString(
              "gender", jsonResponse['response']['gender'].toString());

          showInSnackBarNew(jsonResponse['message']);

          setState(() {
            _isLoading = false;
          });

          // print("TOKEN=======>");
          // String tokenString = "";
          // Future<String> authToken = preferenceManager.getAuthToken();
          // authToken.then((data) {
          //   print("authToken " + data.toString());
          // }, onError: (e) {
          //   print(e);
          // });
          Navigator.of(context).pushAndRemoveUntil(
              // MaterialPageRoute(builder: (BuildContext context) => HomePage()),
              MaterialPageRoute(
                  builder: (BuildContext context) => LaunchProjectPage()),
              (Route<dynamic> route) => false);
        } else {
          setState(() {
            _isLoading = false;
          });
          showInSnackBarNew(jsonResponse['message']);
          print(response.body);
        }
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      print(response.body);
    }
  }

  // on SocketException {
  //   _apiResponse.ApiError = ApiError(error: "Server error. Please retry");
  // }
  // return _apiResponse;
  // }

  void showToast(String message) {
    print("showToast  Called");

    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 12.0);
  }

  void showInSnackBar(String message) {
    new SnackBar(
      duration: Duration(seconds: 30),
      content: Row(
        children: <Widget>[CircularProgressIndicator(), Text(message)],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Username Required';
      },
      onSaved: (val) => _username = val,
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.email),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final passwordField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Password Required';
      },
      obscureText: true,
      style: style,
      onSaved: (val) => _password = val,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.lock),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
            borderSide: BorderSide(width: 1, color: MyColors.kPrimaryColor),
          ),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff003481),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          _handleSubmitted();
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    Widget _buildForgotPasswordBtn() {
      return Container(
        alignment: Alignment.centerRight,
        child: FlatButton(
          onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyForgotPasswordPage()),
          ),
          padding: EdgeInsets.only(right: 0.0),
          child: Text(
            'Forgot Password?',
            style: TextStyle(fontSize: 12),
          ),
        ),
      );
    }

    Widget _buildRememberMeCheckbox() {
      return Container(
        height: 20.0,
        margin: const EdgeInsets.only(bottom: 10.0),
        child: Row(
          children: <Widget>[
            Theme(
              data: ThemeData(unselectedWidgetColor: Colors.blue[900]),
              child: Checkbox(
                value: _rememberMe,
                checkColor: Colors.blue[900],
                activeColor: Colors.white,
                onChanged: (value) {
                  setState(() {
                    _rememberMe = value;
                  });
                },
              ),
            ),
            Text(
              'Remember me',
              style: TextStyle(fontSize: 12),
            ),
          ],
        ),
      );
    }

    Widget _buildSocialBtn(Function onTap, AssetImage logo) {
      return GestureDetector(
        onTap: onTap,
        child: Container(
          height: 60.0,
          width: 60.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                offset: Offset(0, 2),
                blurRadius: 6.0,
              ),
            ],
            image: DecorationImage(
              image: logo,
            ),
          ),
        ),
      );
    }

    Widget _buildSignInWithText() {
      return Column(
        children: <Widget>[
          Text(
            '- OR -',
            style: TextStyle(
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(height: 10.0),
          Text(
            'Sign in with',
            style: style,
          ),
        ],
      );
    }

    Widget _buildSocialBtnRow() {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 20.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _buildSocialBtn(
              () => print('Login with Facebook'),
              AssetImage(
                'assets/images/facebook.jpg',
              ),
            ),
            _buildSocialBtn(
              () => print('Login with Google'),
              AssetImage(
                'assets/images/google.jpg',
              ),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: Center(
          child: SingleChildScrollView(
            child:
                Container(
              child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 50.0,
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Image.asset(
                        "assets/images/specedoor.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                    Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 20.0),
                                emailField,
                                SizedBox(height: 15.0),
                                passwordField,
                              ]),
                        )),
                    _buildForgotPasswordBtn(),
                    _buildRememberMeCheckbox(),
                    loginButon,
                    // SizedBox(height: 20.0),
                    // Container(
                    //   child: new Column(
                    //     children: <Widget>[
                    //       _buildSignInWithText(),
                    //       _buildSocialBtnRow()
                    //     ],
                    //   ),
                    // ),
                    SizedBox(
                      height: 50.0,
                      child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                  child: Text('Don\'t have an account? '),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SignUpPage()),
                                    );
                                  }),
                              GestureDetector(
                                  child: Text(
                                    'Sign Up',
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SignUpPage()),
                                    );
                                  }),
                            ],
                          )),
                    )
                  ],
                ),
              ),
            ),
          ),
          // ),
        ),
    );
  }
}
