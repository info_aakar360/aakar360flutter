import 'dart:collection';
import 'dart:convert';

import 'package:aakar360flutter/Common/customDialog.dart';
import 'package:aakar360flutter/Model/projectStatusModel.dart';
import 'package:aakar360flutter/res/ApiClass.dart';
import 'package:aakar360flutter/res/colors.dart';
import 'package:aakar360flutter/src/launchProjectPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import 'package:http/http.dart' as http;

import '../login_page.dart';

class SelectProjectFilterPage extends StatefulWidget {
  SelectProjectFilterPage({Key key}) : super(key: key);

  @override
  _SelectProjectFilterPageState createState() =>
      _SelectProjectFilterPageState();
}

class _SelectProjectFilterPageState extends State<SelectProjectFilterPage> {
  String _statusCount;
  bool isLoading = false;
  List<ProjectStatusModel> _projectStatusList = new List();
  final Widget errorIcon = new SvgPicture.asset(
    'assets/Icon/close.svg',
    width: 40,
    height: 40,
  );
  final Widget successIcon = new SvgPicture.asset(
    'assets/Icon/check.svg',
    width: 40,
    height: 40,
  );

  void _clearAllFilters() {
    print("ClearAllFilter Called");
    if (_projectStatusList != null) {
      if (_projectStatusList.length > 0) {
        for (int i = 0; i < _projectStatusList.length; i++) {
          setState(() {
            _projectStatusList[i].checked = false;
          });
          print(_projectStatusList[i].name);
          print(_projectStatusList[i].isChecked);
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _getProjectStatusList();
  }

  Future<void> _getProjectStatusList() async {
    try {
      setState(() {
        isLoading = true;
      });

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      final jsonResponse = await http.post(ApiClass.getProjectStatusListApi, body: {
        'API_KEY': ApiClass.API_KEY,
        'token': sharedPreferences.getString('token'),
        // 'project_id': "1",
      });
      Map data = new HashMap();
      data['API_KEY'] = ApiClass.API_KEY;
      // data['project_id'] = "1";
      data['token'] = sharedPreferences.getString('token');
      print(data);
      print(ApiClass.getProjectListApiURL);
      print(jsonResponse.body.toString());
      if (jsonResponse.statusCode == 200) {
        var message = json.decode(jsonResponse.body)['message'];
        String status = json.decode(jsonResponse.body)['status'].toString();
        if (status == "200") {
          print(status);
          final jsonItems = json
              .decode(jsonResponse.body)['responselist']
              .cast<Map<String, dynamic>>();

          _projectStatusList = jsonItems.map<ProjectStatusModel>((json) {
            return ProjectStatusModel.fromJson(json);
          }).toList();
          setState(() {
            isLoading = false;
          });

          setState(() {
            if (_projectStatusList != null && _projectStatusList.length > 0) {
              _statusCount = _projectStatusList.length.toString() + ' Status';

              for (int i = 0; i < _projectStatusList.length; i++) {
                if (_projectStatusList[i].id == 5 ||
                    _projectStatusList[i].id == 6) {
                  _projectStatusList[i].checked = false;
                }
              }
            }
          });
          // showInSnackBar("Data fetched please wait.", successIcon);
        } else if (status == "401") {
          sharedPreferences.clear();
          showInSnackBar("Please Login Again", errorIcon);
          Navigator.of(context).pushReplacement(
              // MaterialPageRoute(builder: (context) => HomePage()))
              MaterialPageRoute(builder: (context) => LoginPage()));
          setState(() {
            isLoading = false;
          });
        } else {
          setState(() {
            isLoading = false;
          });
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => CustomDialogBox(
              title: "Error",
              descriptions: message,
              text: "OK",
            ),
          );
        }
      } else {
        showInSnackBar("Failed to load data.", errorIcon);
        setState(() {
          isLoading = false;
        });
      }
    } on Exception catch (e) {
      print('getProjectList===> $e');
    }
  }

  void showInSnackBar(String value, Widget icon) {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
      duration: Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      backgroundColor: MyColors.kDarkBlue,
      content: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0.0),
        child: Row(
          children: <Widget>[
            icon,
            Container(
                padding: EdgeInsets.only(left: 10.0),
                child: Text(
                  value,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ))
          ],
        ),
      ),
    ));
  }

  void _applyFilter() {
    print("Apply Filter Called");
    List<String> checkedArray = new List<String>();
    for (int i = 0; i < _projectStatusList.length; i++) {
      if (_projectStatusList[i].isChecked) {
        checkedArray.add(_projectStatusList[i].id.toString());
      }
    }
    String strChecked = checkedArray.join(",");
    print(strChecked);
    Navigator.pop(context, strChecked);
    // Navigator.pushReplacement(
    //     context,
    //     MaterialPageRoute(
    //         builder: (BuildContext context) =>
    //             LaunchProjectPage(filter: strChecked)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.kBackgroundColor,
      appBar: AppBar(
        backgroundColor: MyColors.kPrimaryColor,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Filter',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins'),
            ),
            Text(
              _statusCount ?? 'Status',
              style: TextStyle(
                  color: Colors.white, fontSize: 14, fontFamily: 'Poppins'),
            ),
          ],
        ),

        automaticallyImplyLeading: false,
        // this will hide Drawer hamburger icon
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 8.0),
            child: TextButton(
                onPressed: () {
                  _clearAllFilters();
                },
                child: Text(
                  'Clear All',
                  style: TextStyle(
                      color: Colors.white, fontSize: 14, fontFamily: 'Poppins'),
                )),
          ),
        ],
        iconTheme: new IconThemeData(color: Colors.white),
      ),
      body: (isLoading == true && _projectStatusList.length == 0)
          ? Center(child: CircularProgressIndicator())
          : Container(
              child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: _projectStatusList.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0),
                        child: Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: ListTile(
                            title: Text(
                              _projectStatusList[index].name,
                              style: TextStyle(fontSize: 14.0),
                            ),
                            trailing: Theme(
                              data: ThemeData(
                                  unselectedWidgetColor: Colors.blue[900]),
                              child: Checkbox(
                                value: _projectStatusList[index].isChecked,
                                checkColor: Colors.blue[900],
                                activeColor: Colors.white,
                                onChanged: (value) {
                                  setState(() {
                                    _projectStatusList[index].checked = value;
                                  });
                                },
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(right: 10.0, left: 10.0),
                        child: TextButton(
                            style: TextButton.styleFrom(
                              primary: MyColors.kPrimaryColor,
                              side: BorderSide(color: MyColors.kPrimaryColor, width: 2),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text(
                              'Close',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: MyColors.kGreyColor),
                            )),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                          right: 10.0,
                        ),
                        child: TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.white,
                              backgroundColor: MyColors.kPrimaryColor,
                              onSurface: Colors.grey,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                            ),
                            onPressed: () {
                              _applyFilter();
                            },
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                      ),
                    )
                  ],
                )
              ],
            )),
    );
  }
}
