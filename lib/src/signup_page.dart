import 'package:aakar360flutter/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

enum GenderRadio { male, female }

class _SignUpPageState extends State<SignUpPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GenderRadio _site = GenderRadio.male;

  @override
  Widget build(BuildContext context) {
    final nameField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Please enter some text';
      },
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.person),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Full Name",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final phoneField = TextFormField(
      maxLength: 10,
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Please enter some text';
      },
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          counterText: '',
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.phone_android),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Phone",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
    );

    final emailField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Please enter some text';
      },
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.email),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Please enter some text';
      },
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.lock),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final confirmPasswordField = TextFormField(
      // ignore: missing_return
      validator: (value) {
        if (value.isEmpty) return 'Please enter some text';
      },
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.lock),
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Confirm Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff003481),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _scaffoldKey.currentState
                .showSnackBar(SnackBar(content: Text('Processing Data')));
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(builder: (context) => HomePage()),
            // );
          }
        },
        child: Text("Submit",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    int _radioValue1 = -1;
    int correctScore = 0;

    void _handleRadioValueChange1(int value) {
      setState(() {
        _radioValue1 = value;

        switch (_radioValue1) {
          case 0:
            Fluttertoast.showToast(
                msg: 'Correct !', toastLength: Toast.LENGTH_SHORT);
            correctScore++;
            break;
          case 1:
            Fluttertoast.showToast(
                msg: 'Try again !', toastLength: Toast.LENGTH_SHORT);
            break;
          case 2:
            Fluttertoast.showToast(
                msg: 'Try again !', toastLength: Toast.LENGTH_SHORT);
            break;
        }
      });
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text('Sign Up')),
      body:
          // Center(
          //   child:
          SingleChildScrollView(
        child: Container(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                child: Column(
                  children: <Widget>[
                    // Center(
                    //   child: Text(
                    //     'Sign Up',
                    //     style: TextStyle(
                    //         fontSize: 30.0, fontFamily: 'Montserrat'),
                    //   ),
                    // ),
                    // Center(
                    //   child:
                    //   Container(
                    //     height: 80.0,
                    //     padding: const EdgeInsets.only(top: 20.0),
                    //     child: Image.asset(
                    //       "assets/images/mosque.png",
                    //       fit: BoxFit.contain,
                    //     ),
                    //   ),
                    // ),
                    Form(
                        key: _formKey,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 20.0),
                              nameField,
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Text('Select Gender:'),
                              ),
                              Column(children: <Widget>[
                                ListTile(
                                  title: const Text('Male'),
                                  leading: Radio(
                                    value: GenderRadio.male,
                                    groupValue: _site,
                                    onChanged: (GenderRadio value) {
                                      setState(() {
                                        _site = value;
                                      });
                                    },
                                  ),
                                ),
                                ListTile(
                                  title: const Text('Female'),
                                  leading: Radio(
                                    value: GenderRadio.female,
                                    groupValue: _site,
                                    onChanged: (GenderRadio value) {
                                      setState(() {
                                        _site = value;
                                      });
                                    },
                                  ),
                                ),
                              ]),
                              SizedBox(height: 20.0),
                              emailField,
                              SizedBox(height: 20.0),
                              phoneField,
                              SizedBox(height: 20.0),
                              passwordField,
                              SizedBox(height: 20.0),
                              confirmPasswordField,
                            ])),
                    SizedBox(
                      height: 20.0,
                    ),
                    loginButon,
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      // ),
    );
  }
}
