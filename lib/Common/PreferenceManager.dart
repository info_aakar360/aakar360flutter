import 'package:shared_preferences/shared_preferences.dart';

class PreferenceManager {
  final String authToken = "auth_token";
  final String launchProjectList = "launchProjectList";
  final String status = "status";
  final String email = "email";
  final String userImageUrl = "userImageUrl";
  final String userId = "userId";
  final String userName = "userName";
  final String companyId = "companyId";
  final String gender = "gender";
  final String mobile = "mobile";

//set data into shared preferences like this
  Future<void> setAuthToken(String authToken) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.authToken, authToken);
  }

//get value from shared preferences
  Future<String> getAuthToken() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String authToken;
    authToken = pref.getString(this.authToken) ?? null;
    return authToken;
  }

  //set data into shared preferences like this
  Future<void> setLaunchProjectList(String launchProjectList) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.launchProjectList, launchProjectList);
  }

//get value from shared preferences
  Future<String> getLaunchProjectList() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String launchProjectList;
    launchProjectList = pref.getString(this.launchProjectList) ?? null;
    return launchProjectList;
  }

  //set data into shared preferences like this
  Future<void> setStatus(String status) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.status, status);
  }

//get value from shared preferences
  Future<String> getStatus() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String status;
    status = pref.getString(this.status) ?? null;
    return status;
  }

  //set data into shared preferences like this
  Future<void> setEmail(String email) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.email, email);
  }

//get value from shared preferences
  Future<String> getEmail() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String email;
    email = pref.getString(this.email) ?? null;
    return email;
  }

  //set data into shared preferences like this
  Future<void> setUserImageUrl(String imageUrl) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.userImageUrl, imageUrl);
  }

//get value from shared preferences
  Future<String> getUserImage() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String userImageUrl;
    userImageUrl = pref.getString(this.userImageUrl) ?? null;
    return userImageUrl;
  }

  //set data into shared preferences like this
  Future<void> setMobile(String mobile) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.mobile, mobile);
  }

//get value from shared preferences
  Future<String> getMobile() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String mobile;
    mobile = pref.getString(this.mobile) ?? null;
    return mobile;
  }

  //set data into shared preferences like this
  Future<void> setUserId(String userId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.userId, userId);
  }

//get value from shared preferences
  Future<String> getUserId() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String userId;
    userId = pref.getString(this.userId) ?? null;
    return userId;
  }

  //set data into shared preferences like this
  Future<void> setUserName(String userName) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(this.userName, userName);
  }

//get value from shared preferences
  Future<String> getUserName() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String userName;
    userName = pref.getString(this.userName) ?? null;
    return userName;
  }
}
